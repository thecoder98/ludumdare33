
extends Node2D

var scene_manager

func _ready():
	scene_manager = self.get_parent()

func _on_play_button_released():
	scene_manager.change_scene("res://Scenes/game.xml")
	
func _on_credits_button_released():
	#pass
	#uncomment this when we make the credit scene
	scene_manager.change_scene("res://Scenes/credits.xml")