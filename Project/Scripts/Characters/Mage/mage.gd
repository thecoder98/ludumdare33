
extends RigidBody2D

var animation_player
var animation_speed = 2
# load all the animations
var walk_left_animation = preload("res://Animations/Characters/Mage/mage_walking_left.xml")
var walk_right_animation = preload("res://Animations/Characters/Mage/mage_walking_right.xml")
var walk_up_animation = preload("res://Animations/Characters/Mage/mage_walking_up.xml")
var idle_animation = preload("res://Animations/Characters/Mage/mage_idle.xml")

var move_speed = 300
var damp = 5

func _ready():
	self.set_process(true)
	
	self.set_linear_damp(damp)
	self.set_angular_damp(damp)
	# get the animation player
	animation_player = self.get_child(0).get_child(0)
	# add all the animations to the animation player
	animation_player.add_animation("walk_left", walk_left_animation)
	animation_player.add_animation("walk_right", walk_right_animation)
	animation_player.add_animation("walk_up", walk_up_animation)
	animation_player.add_animation("idle", idle_animation)

func _process(delta):
	update_input(delta)

func update_input(delta):
	if(Input.is_action_pressed("ui_up")):
		# play the animation if the it isnt already playing
		if(animation_player.get_current_animation() != "walk_up" || !animation_player.is_playing()):
			animation_player.play("walk_up", -1, animation_speed, false)
			animation_player.get_animation(animation_player.get_current_animation()).set_loop(true)
		self.apply_impulse(Vector2(0, 0), Vector2(0, -move_speed * delta));
	elif(Input.is_action_pressed("ui_down")):
		# play the idle animation if it isnt already playing
		if(animation_player.get_current_animation() != "idle" || !animation_player.is_playing()):
			animation_player.play("idle", -1, animation_speed, false)
			animation_player.get_animation(animation_player.get_current_animation()).set_loop(true)
		self.apply_impulse(Vector2(0, 0), Vector2(0, move_speed * delta))
	elif(Input.is_action_pressed("ui_right")):
		# play the walk_right animation if its not already playing
		if(animation_player.get_current_animation() != "walk_right" || !animation_player.is_playing()):
			animation_player.play("walk_right", -1, animation_speed, false)
			animation_player.get_animation(animation_player.get_current_animation()).set_loop(true)
		self.apply_impulse(Vector2(0, 0), Vector2(move_speed * delta, 0))
	elif(Input.is_action_pressed("ui_left")):
		# play the walk_left animation if its not already playing
		if(animation_player.get_current_animation() != "walk_left" || !animation_player.is_playing()):
			animation_player.play("walk_left", -1, animation_speed, false)
			animation_player.get_animation(animation_player.get_current_animation()).set_loop(true)
		self.apply_impulse(Vector2(0, 0), Vector2(-move_speed * delta, 0))
	elif(animation_player.get_animation(animation_player.get_current_animation()).has_loop()):
		animation_player.get_animation(animation_player.get_current_animation()).set_loop(false)

