
extends Node2D

var current_scene = null

func _ready():
	change_scene("res://Scenes/menu.xml")

func change_scene(scene_path):
	if(current_scene != null):
		current_scene.queue_free()
	current_scene = load(scene_path).instance()
	self.add_child(current_scene)

